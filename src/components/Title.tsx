const Title = ({ title }: { title: string }): JSX.Element => {
  return <p className='text-3xl mb-4'>{title}</p>
}
export default Title
