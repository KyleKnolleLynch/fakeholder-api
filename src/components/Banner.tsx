import Container from './Container'

const Banner = () => {
  return (
    <Container>
      <h1 className='mb-10 text-4xl sm:text-6xl'>{`{JSON} Fakeholder`}</h1>
      <p className='text-2xl'>
        Free fake and reliable API for testing and prototyping.
      </p>
      <p className='mt-2 text-2xl'>
        Powered by <span className='font-medium underline'>JSON Server</span> +{' '}
        <span className='font-medium underline'>LowDB</span>.
      </p>
      <p className='mt-5 font-bold'>Serving ~3 billion requests per month.</p>
    </Container>
  )
}
export default Banner
