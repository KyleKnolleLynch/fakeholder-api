'use client'

import { useState } from 'react'
import Container from './Container'
import Title from './Title'

const Testing = () => {
  const [test, setTest] = useState(false)
  return (
    <Container>
      <Title title='Try it' />
      <p className='mb-4'>Run this code here, in a console or from any site:</p>
      <div className='rounded-lg bg-cyan-900 p-3 text-xs tracking-wider text-orange-200 sm:p-10 sm:text-base'>
        <p>
          <span className='text-sky-300'>fetch</span>
          {`('https://jsonfakeholder.reactdb.com/api/posts/1')`}
        </p>

        <p className='ml-12'>
          <span className='text-sky-300'>.then</span>
          {`(response => response.json())`}
        </p>

        <p className='ml-12'>
          <span className='text-sky-300'>.then</span>
          {`(json => console.log(json))`}
        </p>
      </div>
      <button
        className='my-5 rounded-md bg-black px-4 py-2 text-sm font-semibold text-slate-50 duration-150 hover:bg-black/80 hover:text-white'
        onClick={() => setTest(true)}
      >
        Run script
      </button>
      <div className='rounded-lg bg-cyan-900 p-3 text-xs tracking-wider text-orange-200 sm:p-10 sm:text-base'>
        {test ? (
          <>
            <p>{`{`}</p>
            <p className='ml-5'>
              <span className='font-semibold text-red-500'>
                &quot;userId&quot;
              </span>{' '}
              : <span className='font-semibold text-purple-500'>1</span>,
            </p>
            <p className='ml-5'>
              <span className='font-semibold text-red-500'>&quot;id&quot;</span>{' '}
              : <span className='font-semibold text-purple-500'>1</span>,
            </p>
            <p className='ml-5'>
              <span className='font-semibold text-red-500'>
                &quot;title&quot;
              </span>{' '}
              :{' '}
              <span className='font-medium text-orange-200'>
                &quot;delectus aut autem&quot;
              </span>
              ,
            </p>
            <p className='ml-5'>
              <span className='font-semibold text-red-500'>
                &quot;completed&quot;
              </span>{' '}
              : <span className='font-semibold text-purple-500'>true</span>,
            </p>
            <p>{`}`}</p>
          </>
        ) : (
          <p>{`{}`}</p>
        )}
      </div>
      {test && (
        <p className='mt-5 text-sm font-medium'>
          Congratulations! You&apos;ve made your first call to JSONFakeholder.
        </p>
      )}
    </Container>
  )
}
export default Testing
