import Container from './Container'
import Title from './Title'

const WhenToUse = () => {
  return (
    <Container>
      <Title title='When to use' />
      <p className='mt-5 text-sm leading-6'>
        JSONFakeholder is a free online REST API that you can use{' '}
        <span className='font-semibold'>whenever you need some fake data</span>.
        It can be in a README on GitHub, for a demo on CodeSandbox, in code
        examples on Stack Overflow, ...or simply to test things locally.
      </p>
    </Container>
  )
}
export default WhenToUse
