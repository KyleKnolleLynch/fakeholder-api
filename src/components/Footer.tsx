import Container from './Container'

const Footer = () => {
  return (
    <footer>
      <Container>
        <p className='text-sm'>
          Coded and maintained with ❤️ by Kyle Lynch @ 2024
        </p>
      </Container>
    </footer>
  )
}
export default Footer
