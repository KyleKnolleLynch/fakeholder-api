import Link from 'next/link'
import Container from './Container'

const navigation = [
  {
    title: 'Guide',
    href: '/guide',
  },
  {
    title: 'Sponsor this project',
    href: '/',
  },
  {
    title: 'Blog',
    href: '/blog',
  },
  {
    title: 'My JSON Server',
    href: '/server',
  },
  {
    title: 'Playground',
    href: '/playground',
  },
]

const Header = () => {
  return (
    <nav className='sticky top-0 z-50 border-b border-b-gray-500 bg-white shadow-sm'>
      <Container className='flex flex-col items-center justify-between gap-2 sm:flex-row'>
        <Link href='/'>
          <p className='font-bold'>JSON Fakeholder</p>
        </Link>
        <menu className='flex flex-col items-start sm:flex-row sm:space-x-4'>
          {navigation.map(item => (
            <li key={item.title}>
              <Link href={item.href} className='hover:text-indigo-600'>
                {item.title}
              </Link>
            </li>
          ))}
        </menu>
      </Container>
    </nav>
  )
}
export default Header
