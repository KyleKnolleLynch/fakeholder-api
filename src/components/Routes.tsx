import Link from 'next/link'
import Container from './Container'
import Title from './Title'

const routesArr = [
  {
    href: '/api/posts',
    total: 'GET',
  },
  {
    href: '/api/posts/1',
    total: 'GET',
  },
  {
    href: '/posts/1/comments',
    total: 'GET',
  },
  {
    href: '/comments?postId=1',
    total: 'GET',
  },
  {
    href: '/posts',
    total: 'POST',
  },
  {
    href: '/posts/1',
    total: 'PUT',
  },
  {
    href: '/posts/1',
    total: 'PATCH',
  },
  {
    href: '/posts/1',
    total: 'DELETE',
  },
]

const Routes = () => {
  return (
    <Container>
      <Title title='Routes' />
      <p className='my-5 text-sm'>
        All HTTP methods are supported. You can use http or https for your
        requests.
      </p>
      <div className='my-5 flex flex-col gap-y-2'>
        {routesArr.map(item => (
          <div
            key={item?.href}
            className='flex w-72 items-center justify-between'
          >
            <p>{item?.total}</p>
            <Link href={item?.href} className='w-40 text-left'>
              {item?.href}
            </Link>
          </div>
        ))}
      </div>
    </Container>
  )
}
export default Routes