import { NextResponse } from "next/server"
import { postsData } from "@/data/posts"

export const GET = () => {
    const singlePost = postsData.find(item => item?.id === 1)
    return NextResponse.json({
        message: 'Connection established successfully!',
        success: true,
        data: singlePost
    })
}