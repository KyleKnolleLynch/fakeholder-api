import { NextResponse } from 'next/server'
import {postsData} from '@/data/posts'

export const GET = () => {
  return NextResponse.json({
    message: 'Data fetched successfully!',
    success: true,
    data: postsData
  })
}
